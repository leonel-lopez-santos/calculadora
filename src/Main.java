import java.util.Scanner;

public class Main {

    static final int SUMA = 1;
    static final int RESTA = 2;
    static final int DIVISION = 3;
    static final int MULTIPLICACION = 4;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int opcion = menu(scanner);
        float numero1 = obtenerNumero(scanner);
        float numero2 = obtenerNumero(scanner);
        if (opcion == SUMA) {
            float suma = suma(numero1,numero2);
            System.out.println("La suma es: " + suma);
        } else if (opcion == RESTA){
            float resta = resta(numero1, numero2);
            System.out.println("La resta es : " + resta);
        } else if (opcion == MULTIPLICACION){
            float multiplicacion = multiplicacion(numero1, numero2);
            System.out.println("La multiplicación es: " + multiplicacion);
        } else if (opcion == DIVISION){
            float division = division(numero1,numero2);
            System.out.println("La división es: " + division);
        }
    }

    static int menu(Scanner scanner) {
        System.out.println("Introduzca una opción:");
        System.out.println("\t1-Suma");
        System.out.println("\t2-Resta");
        System.out.println("\t3-División");
        System.out.println("\t4-Multiplicación");

        return scanner.nextInt();
    }

    static float suma(float numero1, float numero2){
        return numero1 + numero2;
    }

    static float obtenerNumero(Scanner scanner){
        System.out.println("Introduzca un número:");
        return scanner.nextFloat();
    }

    static float resta(float numero1, float numero2){
        return numero1 - numero2;
    }

    static float multiplicacion(float numero1, float numero2) { return numero1*numero2; }

    static float division(float numero1, float numero2) { return numero1/numero2;}
}
